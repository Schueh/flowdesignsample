﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowDesignSample
{
    class Program
    {
        static void Main(string[] args)
        {
            string a = OperationOne();
            object b = OperationTwo(a);
            OperationThree(b);
            int c = OperationFour(a, b);

            Console.Write(c);
        }

        static string OperationOne()
        {
            return string.Empty;
        }

        static object OperationTwo(string data)
        {
            return new object();
        }

        static void OperationThree(object data)
        {
            
        }

        static int OperationFour(string data, object anotherData)
        {
            return 0;
        }
    }
}
